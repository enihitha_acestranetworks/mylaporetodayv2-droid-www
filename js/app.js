var app_id='NBpeKz9Ukpy9LHpZJcHSZJWsQ';
var model={};
var device_token='';
var backbutton=0;
var set={};
var get_token='';
var app=angular.module('mylapore', ['ionic','ngCordova','ionic.rating','angular-momentjs','ionic-native-transitions','yaru22.angular-timeago'])

app.run(function($ionicPlatform,acepush,mylaipush,$cordovaToast,$state,$ionicHistory,$timeout) {
  $ionicPlatform.ready(function() {
    if(!localStorage.getItem('setting') || localStorage.getItem('setting') == null ){
      set={'init':true,alert:true,font_size:'Medium'};
      localStorage.setItem('setting',JSON.stringify(set));
    }
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    if(window.Connection) {
    if(navigator.connection.type == Connection.NONE) {
      $cordovaToast.showLongBottom('Sorry, no Internet connectivity detected. Please reconnect and try again.');
   }
  }
  model=ionic.Platform.device();

  var push = PushNotification.init({"android":{ "senderID": "818195329011"}});
  push.on('registration', function(data) {
    device_token=data.registrationId;
    get_token=JSON.parse(localStorage.getItem('device_token'));
    if(get_token != device_token || !get_token || get_token=='' || get_token==null){
      localStorage.setItem('device_token',JSON.stringify(device_token));
      var obj={'app_id':app_id,'device_token':device_token,'model':model.model,'platform':model.platform,'status':true}
        acepush.notification(obj);
        mylaipush.notification(obj);
    }
  });

  push.on('notification', function(data) {
    var a=data.additionalData.openUrl.split("_");
    if(a[0]=="notification"){
      $state.go('details',{'obj':{'page':a[0],'category':a[1],'id':a[2]}});
    }
  });
  });
})

app.config(function($stateProvider,$urlRouterProvider,$ionicConfigProvider,$httpProvider,$momentProvider,$ionicNativeTransitionsProvider) {
  $ionicNativeTransitionsProvider.setDefaultOptions({
       duration: 300, // in milliseconds (ms), default 400,
       slowdownfactor: 3, // overlap views (higher number is more) or no overlap (1), default 4
       iosdelay: -1, // ms to wait for the iOS webview to update before animation kicks in, default -1
       androiddelay: -1, // same as above but for Android, default -1
       winphonedelay: -1, // same as above but for Windows Phone, default -1,
       fixedPixelsTop: 0, // the number of pixels of your fixed header, default 0 (iOS and Android)
       fixedPixelsBottom: 0, // the number of pixels of your fixed footer (f.i. a tab bar), default 0 (iOS and Android)
       triggerTransitionEvent: '$ionicView.afterEnter', // internal ionic-native-transitions option
       backInOppositeDirection: true // Takes over default back transition and state back transition to use the opposite direction transition to go back
   });
$ionicNativeTransitionsProvider.setDefaultTransition({"type":"fade"});
$ionicConfigProvider.views.transition('none');
$ionicConfigProvider.tabs.position('top'); // other values: top
$ionicConfigProvider.backButton.previousTitleText(false).text('');
// $ionicConfigProvider.views.transition('none');
$ionicConfigProvider.scrolling.jsScrolling(false);
// $ionicSideMenuDelegate.canDragContent(false);
 $httpProvider.defaults.useXDomain = true;
  // Remove the header used to identify ajax call  that would prevent CORS from working
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
  // Set api token
  if(typeof API_TOKEN !== 'undefined') {
    $httpProvider.defaults.headers.common['Authentication-Token'] = API_TOKEN;
  }
  $momentProvider
      .asyncLoading(false)
      .scriptUrl('moment.min.js');
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/tab.html',
    controller: 'AppCtrl'
  })
  .state('app.news', {
    url: '/news',
    views: {
      'tab-news': {
        templateUrl: 'templates/news.html',
        controller: 'NewsCtrl'
      }
    }
  })
  .state('app.classified', {
    url: '/classified',
    views: {
      'tab-classified': {
        templateUrl: 'templates/classified.html',
        controller: 'ClassifiedCtrl'
      }
    }
  })
  .state('app.directory', {
    url: '/directory',
    views: {
      'tab-directory': {
        templateUrl: 'templates/directory.html',
        controller: 'DirectoryCtrl'
      }
    }
  })
  .state('app.civic', {
    url: '/civic',
    views: {
      'tab-civic': {
        templateUrl: 'templates/civic.html',
        controller: 'CivicCtrl'
      }
    }
  })
  .state('app.event', {
    url: '/event',
    views: {
      'tab-event': {
        templateUrl: 'templates/event.html',
        controller: 'EventCtrl'
      }
    }
  })
  .state('app.tourism', {
    url: '/tourism',
    views: {
      'tab-tourism': {
        templateUrl: 'templates/tourism.html',
        controller: 'TourismCtrl'
      }
    }
  })
  .state('contact', {
    url: '/contact',
    templateUrl: 'templates/contact.html',
    controller: 'ContactCtrl'
  })
  .state('search', {
    url: '/search',
    params:{obj:null},
    cache:false,
    nativeTransitionsBackAndroid: {
      "type": "slide",
      "direction": "down"
    },
    nativeTransitions:{
          "type": "slide",
          "direction": "up"
    },
    templateUrl: 'templates/details.html',
    controller: 'SearchCtrl'
  })
  .state('vilambaram', {
    url: '/vilambaram',
    templateUrl: 'templates/vilambaram.html',
    controller: 'VilambaramCtrl'
  })

  .state('settings', {
    url: '/settings',
    cache:false,
    templateUrl: 'templates/settings.html',
    controller: 'SettingsCtrl'
  })
  .state('details', {
    url: '/details',
    params:{obj:null},
    nativeTransitionsBackAndroid: {
      "type": "slide",
      "direction": "down"
    },
    nativeTransitions:{
          "type": "slide",
          "direction": "up"
    },
    templateUrl: 'templates/details.html',
    controller: 'DetailsCtrl'
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/news');
});

var url='https://www.mylaporetoday.com/api/';
var image_url='https://www.mylaporetoday.com/';
/*var share_url = 'https://www.mylaporetoday.com/local-area-news-details/';*/
// var url='http://v4mylai.acecommunicate.com/welcome/';
// var image_url='http://v4mylai.acecommunicate.com/';
// var url='http://mylaievent/welcome/';
// var image_url='http://mylaievent/';
var ad=0;
var limit=17;

/*var share_url;
var cat=datas.category;
if( cat == 'News')
{
share_url = 'https://www.mylaporetoday.com/local-area-news-details/';
}
else if( cat == 'Event')
{
share_url = 'https://www.mylaporetoday.com/event_sub/'
}
else if( cat == 'Civic Issue')
{
share_url = 'https://www.mylaporetoday.com/civic-issues-details/'
}
else if( cat == 'Tourism')
{
share_url = 'https://www.mylaporetoday.com/tourism_sub/'
}*/